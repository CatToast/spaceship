﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveTowardsShip : MonoBehaviour
{
    //public Transform target; this was used to drop in the ship prefab

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        // look at the ship

        Spaceship spaceShip = FindObjectOfType<Spaceship>();

        Vector3 target = spaceShip.transform.position;

        transform.LookAt(target);

       
        // Draw a ray for debug purposes
        Vector3 forward = transform.TransformDirection(Vector3.forward) * 10;
        Debug.DrawRay(transform.position, forward, Color.green, 30, false);

    }
    

}
