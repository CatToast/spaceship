﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour
{
    public GameObject enemy;
    public GameObject ship;
    public float spawnClock; // how long it has been since last spawn
    public float spawnWait; // editable number of seconds to add to spawn counter
    //public float waitTime;

 
    // Start is called before the first frame update
    void Start()
    {
        

        Vector3 pos2 = new Vector3(0f, 0f, 0f); // instantiate the ship
        Instantiate(ship, pos2, Quaternion.identity);

        
    }

    // Update is called once per frame
    void Update()
    {
        //InvokeRepeating("EnemySpawner", waitTime, spawnTime); // not working. interested to learn why

        if (Time.time > spawnClock)
        {
            spawnClock += spawnWait;
            Vector3 pos = new Vector3(Random.Range(-10.0f, 10.0f), 0f, Random.Range(10.0f, 20.0f));
            Instantiate(enemy, pos, Quaternion.identity);

        }

    }

    //void EnemySpawner()
    //{
        //Vector3 pos = new Vector3(Random.Range(-10.0f, 10.0f), 0f, Random.Range(10.0f, 20.0f));
        //Instantiate(enemy, pos, Quaternion.identity);
    //}
}
