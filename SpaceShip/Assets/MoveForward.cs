﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveForward : MonoBehaviour


{
    public float speed = 1f; // editor values override code values. if you dont change it in editor, it will use code
  

    // Update is called once per frame
    void Update()
    {
        transform.Translate(0f, 0f, speed*Time.deltaTime);
    }

   
}
