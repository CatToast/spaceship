﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyEnemy : MonoBehaviour

    

{
    public GameObject explosionParticles;
    


    void OnCollisionEnter(Collision collision)
        {

        if (collision.gameObject.tag == "Bullet")
        {

            Debug.Log("Pew Pew");
            Destroy(collision.gameObject);
            Instantiate(explosionParticles, transform.position, Quaternion.identity);
            Destroy(gameObject);
            
          

        

        }

        else
        {
            Debug.Log("Yikes!");
            Destroy(collision.gameObject);
            Instantiate(explosionParticles, transform.position, Quaternion.identity);
            Destroy(gameObject);
            

        }
           
     
            
        }
    
}
