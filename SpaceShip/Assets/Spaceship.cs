﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spaceship : MonoBehaviour
{

    public GameObject bullet;
    public float turnSpeed = 150f;

    // Update is called once per frame
    void Update()
    {
        //fire the bullet with Space
        if (Input.GetKeyDown(KeyCode.Space))
        {
            Quaternion rot = transform.rotation;
            Instantiate(bullet, transform.position, rot); // make the object - vector3.zero means 0f,0f,0f, grabs transform of the object script is attached to 
        }

        //rotate left
        if (Input.GetKey(KeyCode.LeftArrow))
        {
            transform.Rotate(Vector3.up, -turnSpeed * Time.deltaTime);
        }

        //rotate right
        if (Input.GetKey(KeyCode.RightArrow))
        {
            transform.Rotate(Vector3.up, turnSpeed * Time.deltaTime);
        }

        
        // Destroy Enemy and Ship

    }
}
